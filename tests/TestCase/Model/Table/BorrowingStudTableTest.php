<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BorrowingStudTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BorrowingStudTable Test Case
 */
class BorrowingStudTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BorrowingStudTable
     */
    public $BorrowingStud;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.borrowing_stud',
        'app.borrow_stud',
        'app.equiptment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BorrowingStud') ? [] : ['className' => BorrowingStudTable::class];
        $this->BorrowingStud = TableRegistry::getTableLocator()->get('BorrowingStud', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BorrowingStud);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
