<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BorrowStudTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BorrowStudTable Test Case
 */
class BorrowStudTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BorrowStudTable
     */
    public $BorrowStud;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.borrow_stud',
        'app.student'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BorrowStud') ? [] : ['className' => BorrowStudTable::class];
        $this->BorrowStud = TableRegistry::getTableLocator()->get('BorrowStud', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BorrowStud);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
