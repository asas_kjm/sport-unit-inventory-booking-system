<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EquiptmentTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EquiptmentTable Test Case
 */
class EquiptmentTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EquiptmentTable
     */
    public $Equiptment;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.equiptment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Equiptment') ? [] : ['className' => EquiptmentTable::class];
        $this->Equiptment = TableRegistry::getTableLocator()->get('Equiptment', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Equiptment);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
