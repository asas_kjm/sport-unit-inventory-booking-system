<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BorrowingStaffTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BorrowingStaffTable Test Case
 */
class BorrowingStaffTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BorrowingStaffTable
     */
    public $BorrowingStaff;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.borrowing_staff',
        'app.equiptment'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BorrowingStaff') ? [] : ['className' => BorrowingStaffTable::class];
        $this->BorrowingStaff = TableRegistry::getTableLocator()->get('BorrowingStaff', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BorrowingStaff);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
