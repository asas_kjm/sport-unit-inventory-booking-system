<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BorrowStaffTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BorrowStaffTable Test Case
 */
class BorrowStaffTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BorrowStaffTable
     */
    public $BorrowStaff;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.borrow_staff',
        'app.staff'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BorrowStaff') ? [] : ['className' => BorrowStaffTable::class];
        $this->BorrowStaff = TableRegistry::getTableLocator()->get('BorrowStaff', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BorrowStaff);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
