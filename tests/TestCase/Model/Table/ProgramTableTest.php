<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProgramTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProgramTable Test Case
 */
class ProgramTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProgramTable
     */
    public $Program;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.program'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Program') ? [] : ['className' => ProgramTable::class];
        $this->Program = TableRegistry::getTableLocator()->get('Program', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Program);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
