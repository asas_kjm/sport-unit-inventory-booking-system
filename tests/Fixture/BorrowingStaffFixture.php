<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BorrowingStaffFixture
 *
 */
class BorrowingStaffFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'borrowing_staff';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'equipt_id' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'status' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'date' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'retrn' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'id' => ['type' => 'index', 'columns' => ['id'], 'length' => []],
            'equipt_id' => ['type' => 'index', 'columns' => ['equipt_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id', 'equipt_id'], 'length' => []],
            'borrowing_staff_ibfk_1' => ['type' => 'foreign', 'columns' => ['id'], 'references' => ['borrow_staff', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'borrowing_staff_ibfk_2' => ['type' => 'foreign', 'columns' => ['equipt_id'], 'references' => ['equiptment', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => '295f960f-21cd-4538-95a7-eea9ff48318d',
                'equipt_id' => '0451fea2-0564-4558-9264-74573feaf459',
                'status' => 'Lorem ipsum dolor sit amet',
                'date' => '2018-10-07 16:10:11',
                'retrn' => 'Lorem ipsum dolor sit amet'
            ],
        ];
        parent::init();
    }
}
