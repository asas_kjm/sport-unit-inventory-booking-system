<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BorrowingStudFixture
 *
 */
class BorrowingStudFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'borrowing_stud';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'borwstud_id' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'equipt_id' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'status' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'date' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'retrn' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'borwstud_id' => ['type' => 'index', 'columns' => ['borwstud_id'], 'length' => []],
            'equipt_id' => ['type' => 'index', 'columns' => ['equipt_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['borwstud_id', 'equipt_id'], 'length' => []],
            'borrowing_stud_ibfk_1' => ['type' => 'foreign', 'columns' => ['borwstud_id'], 'references' => ['borrow_stud', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
            'borrowing_stud_ibfk_2' => ['type' => 'foreign', 'columns' => ['equipt_id'], 'references' => ['equiptment', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'borwstud_id' => '2d892873-77bd-47c9-9186-46d676fa3909',
                'equipt_id' => '924f034d-6090-4f24-9029-4b79098929c5',
                'status' => 'Lorem ipsum dolor sit amet',
                'date' => '2018-10-07 15:53:21',
                'retrn' => '2018-10-07 15:53:21'
            ],
        ];
        parent::init();
    }
}
