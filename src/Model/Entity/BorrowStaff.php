<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BorrowStaff Entity
 *
 * @property string $id
 * @property \Cake\I18n\FrozenTime $date
 * @property string $staff_id
 *
 * @property \App\Model\Entity\Staff $staff
 */
class BorrowStaff extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date' => true,
        'staff_id' => true,
        'staff' => true
    ];
}
