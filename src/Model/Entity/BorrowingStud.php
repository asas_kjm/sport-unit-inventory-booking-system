<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BorrowingStud Entity
 *
 * @property string $borwstud_id
 * @property string $equipt_id
 * @property string $status
 * @property \Cake\I18n\FrozenTime $date
 * @property \Cake\I18n\FrozenTime $retrn
 *
 * @property \App\Model\Entity\BorrowStud $borrow_stud
 * @property \App\Model\Entity\Equiptment $equiptment
 */
class BorrowingStud extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'status' => true,
        'date' => true,
        'retrn' => true,
        'borrow_stud' => true,
        'equiptment' => true
    ];
}
