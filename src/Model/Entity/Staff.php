<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Staff Entity
 *
 * @property string $id
 * @property string $name
 * @property string $phoneNum
 * @property string $dept_code
 * @property float $salary
 * @property string $position
 * @property string $password
 *
 * @property \App\Model\Entity\BorrowStaff[] $borrow_staff
 */
class Staff extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'phoneNum' => true,
        'dept_code' => true,
        'salary' => true,
        'position' => true,
        'password' => true,
        'borrow_staff' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
