<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property string $id
 * @property string $name
 * @property string $phoneNum
 * @property string $program_code
 * @property int $semester
 * @property string $status
 * @property string $password
 *
 * @property \App\Model\Entity\BorrowStud[] $borrow_stud
 */
class Student extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'phoneNum' => true,
        'program_code' => true,
        'semester' => true,
        'status' => true,
        'password' => true,
        'borrow_stud' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
