<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BorrowStud Model
 *
 * @property \App\Model\Table\StudentTable|\Cake\ORM\Association\BelongsTo $Student
 *
 * @method \App\Model\Entity\BorrowStud get($primaryKey, $options = [])
 * @method \App\Model\Entity\BorrowStud newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BorrowStud[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BorrowStud|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BorrowStud|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BorrowStud patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BorrowStud[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BorrowStud findOrCreate($search, callable $callback = null, $options = [])
 */
class BorrowStudTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('borrow_stud');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Student', [
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->maxLength('id', 255)
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_id'], 'Student'));

        return $rules;
    }
}
