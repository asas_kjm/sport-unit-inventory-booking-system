<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BorrowingStaff Model
 *
 * @property \App\Model\Table\EquiptmentTable|\Cake\ORM\Association\BelongsTo $Equiptment
 *
 * @method \App\Model\Entity\BorrowingStaff get($primaryKey, $options = [])
 * @method \App\Model\Entity\BorrowingStaff newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BorrowingStaff[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BorrowingStaff|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BorrowingStaff|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BorrowingStaff patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BorrowingStaff[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BorrowingStaff findOrCreate($search, callable $callback = null, $options = [])
 */
class BorrowingStaffTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('borrowing_staff');
        $this->setDisplayField('id');
        $this->setPrimaryKey(['id', 'equipt_id']);

        $this->belongsTo('Equiptment', [
            'foreignKey' => 'equipt_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->maxLength('id', 255)
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('retrn')
            ->maxLength('retrn', 255)
            ->requirePresence('retrn', 'create')
            ->notEmpty('retrn');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['equipt_id'], 'Equiptment'));

        return $rules;
    }
}
