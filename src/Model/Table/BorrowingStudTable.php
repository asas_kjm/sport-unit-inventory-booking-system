<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BorrowingStud Model
 *
 * @property \App\Model\Table\BorrowStudTable|\Cake\ORM\Association\BelongsTo $BorrowStud
 * @property \App\Model\Table\EquiptmentTable|\Cake\ORM\Association\BelongsTo $Equiptment
 *
 * @method \App\Model\Entity\BorrowingStud get($primaryKey, $options = [])
 * @method \App\Model\Entity\BorrowingStud newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BorrowingStud[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BorrowingStud|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BorrowingStud|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BorrowingStud patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BorrowingStud[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BorrowingStud findOrCreate($search, callable $callback = null, $options = [])
 */
class BorrowingStudTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('borrowing_stud');
        $this->setDisplayField('borwstud_id');
        $this->setPrimaryKey(['borwstud_id', 'equipt_id']);

        $this->belongsTo('BorrowStud', [
            'foreignKey' => 'borwstud_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Equiptment', [
            'foreignKey' => 'equipt_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('status')
            ->maxLength('status', 255)
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->dateTime('retrn')
            ->requirePresence('retrn', 'create')
            ->notEmpty('retrn');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['borwstud_id'], 'BorrowStud'));
        $rules->add($rules->existsIn(['equipt_id'], 'Equiptment'));

        return $rules;
    }
}
