<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $department
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $department->code],
                ['confirm' => __('Are you sure you want to delete # {0}?', $department->code)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Department'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="department form large-9 medium-8 columns content">
    <?= $this->Form->create($department) ?>
    <fieldset>
        <legend><?= __('Edit Department') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
