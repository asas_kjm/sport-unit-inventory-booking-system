<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowStud $borrowStud
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Borrow Stud'), ['action' => 'edit', $borrowStud->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Borrow Stud'), ['action' => 'delete', $borrowStud->id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowStud->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrow Stud'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Student'), ['controller' => 'Student', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Student', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="borrowStud view large-9 medium-8 columns content">
    <h3><?= h($borrowStud->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($borrowStud->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Student') ?></th>
            <td><?= $borrowStud->has('student') ? $this->Html->link($borrowStud->student->name, ['controller' => 'Student', 'action' => 'view', $borrowStud->student->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($borrowStud->date) ?></td>
        </tr>
    </table>
</div>
