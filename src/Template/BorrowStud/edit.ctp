<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowStud $borrowStud
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $borrowStud->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $borrowStud->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Student'), ['controller' => 'Student', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Student'), ['controller' => 'Student', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="borrowStud form large-9 medium-8 columns content">
    <?= $this->Form->create($borrowStud) ?>
    <fieldset>
        <legend><?= __('Edit Borrow Stud') ?></legend>
        <?php
            echo $this->Form->control('date');
            echo $this->Form->control('student_id', ['options' => $student]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
