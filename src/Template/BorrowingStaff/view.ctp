<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowingStaff $borrowingStaff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Borrowing Staff'), ['action' => 'edit', $borrowingStaff->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Borrowing Staff'), ['action' => 'delete', $borrowingStaff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowingStaff->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Borrowing Staff'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrowing Staff'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Equiptment'), ['controller' => 'Equiptment', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Equiptment'), ['controller' => 'Equiptment', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="borrowingStaff view large-9 medium-8 columns content">
    <h3><?= h($borrowingStaff->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($borrowingStaff->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Equiptment') ?></th>
            <td><?= $borrowingStaff->has('equiptment') ? $this->Html->link($borrowingStaff->equiptment->name, ['controller' => 'Equiptment', 'action' => 'view', $borrowingStaff->equiptment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($borrowingStaff->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Retrn') ?></th>
            <td><?= h($borrowingStaff->retrn) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($borrowingStaff->date) ?></td>
        </tr>
    </table>
</div>
