<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowingStaff $borrowingStaff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $borrowingStaff->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $borrowingStaff->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Borrowing Staff'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Equiptment'), ['controller' => 'Equiptment', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Equiptment'), ['controller' => 'Equiptment', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="borrowingStaff form large-9 medium-8 columns content">
    <?= $this->Form->create($borrowingStaff) ?>
    <fieldset>
        <legend><?= __('Edit Borrowing Staff') ?></legend>
        <?php
            echo $this->Form->control('status');
            echo $this->Form->control('date');
            echo $this->Form->control('retrn');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
