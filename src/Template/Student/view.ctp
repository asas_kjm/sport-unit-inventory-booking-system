<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Student'), ['action' => 'edit', $student->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Student'), ['action' => 'delete', $student->id], ['confirm' => __('Are you sure you want to delete # {0}?', $student->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Student'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Student'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="student view large-9 medium-8 columns content">
    <h3><?= h($student->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($student->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($student->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PhoneNum') ?></th>
            <td><?= h($student->phoneNum) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Program Code') ?></th>
            <td><?= h($student->program_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($student->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($student->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Semester') ?></th>
            <td><?= $this->Number->format($student->semester) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Borrow Stud') ?></h4>
        <?php if (!empty($student->borrow_stud)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Student Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($student->borrow_stud as $borrowStud): ?>
            <tr>
                <td><?= h($borrowStud->id) ?></td>
                <td><?= h($borrowStud->date) ?></td>
                <td><?= h($borrowStud->student_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BorrowStud', 'action' => 'view', $borrowStud->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BorrowStud', 'action' => 'edit', $borrowStud->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BorrowStud', 'action' => 'delete', $borrowStud->id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowStud->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
