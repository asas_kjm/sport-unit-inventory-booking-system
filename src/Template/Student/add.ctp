<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Student $student
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Student'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="student form large-9 medium-8 columns content">
    <?= $this->Form->create($student) ?>
    <fieldset>
        <legend><?= __('Add Student') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('phoneNum');
            echo $this->Form->control('program_code');
            echo $this->Form->control('semester');
            echo $this->Form->control('status');
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
