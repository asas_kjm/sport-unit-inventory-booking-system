<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowingStud[]|\Cake\Collection\CollectionInterface $borrowingStud
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Borrowing Stud'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Equiptment'), ['controller' => 'Equiptment', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Equiptment'), ['controller' => 'Equiptment', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="borrowingStud index large-9 medium-8 columns content">
    <h3><?= __('Borrowing Stud') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('borwstud_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('equipt_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('retrn') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($borrowingStud as $borrowingStud): ?>
            <tr>
                <td><?= $borrowingStud->has('borrow_stud') ? $this->Html->link($borrowingStud->borrow_stud->id, ['controller' => 'BorrowStud', 'action' => 'view', $borrowingStud->borrow_stud->id]) : '' ?></td>
                <td><?= $borrowingStud->has('equiptment') ? $this->Html->link($borrowingStud->equiptment->name, ['controller' => 'Equiptment', 'action' => 'view', $borrowingStud->equiptment->id]) : '' ?></td>
                <td><?= h($borrowingStud->status) ?></td>
                <td><?= h($borrowingStud->date) ?></td>
                <td><?= h($borrowingStud->retrn) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $borrowingStud->borwstud_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $borrowingStud->borwstud_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $borrowingStud->borwstud_id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowingStud->borwstud_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
