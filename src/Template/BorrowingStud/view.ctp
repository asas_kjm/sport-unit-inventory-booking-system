<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowingStud $borrowingStud
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Borrowing Stud'), ['action' => 'edit', $borrowingStud->borwstud_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Borrowing Stud'), ['action' => 'delete', $borrowingStud->borwstud_id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowingStud->borwstud_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Borrowing Stud'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrowing Stud'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Equiptment'), ['controller' => 'Equiptment', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Equiptment'), ['controller' => 'Equiptment', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="borrowingStud view large-9 medium-8 columns content">
    <h3><?= h($borrowingStud->borwstud_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Borrow Stud') ?></th>
            <td><?= $borrowingStud->has('borrow_stud') ? $this->Html->link($borrowingStud->borrow_stud->id, ['controller' => 'BorrowStud', 'action' => 'view', $borrowingStud->borrow_stud->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Equiptment') ?></th>
            <td><?= $borrowingStud->has('equiptment') ? $this->Html->link($borrowingStud->equiptment->name, ['controller' => 'Equiptment', 'action' => 'view', $borrowingStud->equiptment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($borrowingStud->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($borrowingStud->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Retrn') ?></th>
            <td><?= h($borrowingStud->retrn) ?></td>
        </tr>
    </table>
</div>
