<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowingStud $borrowingStud
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $borrowingStud->borwstud_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $borrowingStud->borwstud_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Borrowing Stud'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Borrow Stud'), ['controller' => 'BorrowStud', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Equiptment'), ['controller' => 'Equiptment', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Equiptment'), ['controller' => 'Equiptment', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="borrowingStud form large-9 medium-8 columns content">
    <?= $this->Form->create($borrowingStud) ?>
    <fieldset>
        <legend><?= __('Edit Borrowing Stud') ?></legend>
        <?php
            echo $this->Form->control('status');
            echo $this->Form->control('date');
            echo $this->Form->control('retrn');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
