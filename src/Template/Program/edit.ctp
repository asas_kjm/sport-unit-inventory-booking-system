<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Program $program
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $program->code],
                ['confirm' => __('Are you sure you want to delete # {0}?', $program->code)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Program'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="program form large-9 medium-8 columns content">
    <?= $this->Form->create($program) ?>
    <fieldset>
        <legend><?= __('Edit Program') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('level');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
