<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowStaff $borrowStaff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $borrowStaff->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $borrowStaff->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Borrow Staff'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="borrowStaff form large-9 medium-8 columns content">
    <?= $this->Form->create($borrowStaff) ?>
    <fieldset>
        <legend><?= __('Edit Borrow Staff') ?></legend>
        <?php
            echo $this->Form->control('date');
            echo $this->Form->control('staff_id', ['options' => $staff]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
