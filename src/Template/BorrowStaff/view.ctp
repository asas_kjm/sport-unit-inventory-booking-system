<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BorrowStaff $borrowStaff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Borrow Staff'), ['action' => 'edit', $borrowStaff->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Borrow Staff'), ['action' => 'delete', $borrowStaff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowStaff->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Borrow Staff'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrow Staff'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="borrowStaff view large-9 medium-8 columns content">
    <h3><?= h($borrowStaff->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($borrowStaff->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Staff') ?></th>
            <td><?= $borrowStaff->has('staff') ? $this->Html->link($borrowStaff->staff->name, ['controller' => 'Staff', 'action' => 'view', $borrowStaff->staff->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($borrowStaff->date) ?></td>
        </tr>
    </table>
</div>
