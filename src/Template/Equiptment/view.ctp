<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Equiptment $equiptment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Equiptment'), ['action' => 'edit', $equiptment->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Equiptment'), ['action' => 'delete', $equiptment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $equiptment->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Equiptment'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Equiptment'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="equiptment view large-9 medium-8 columns content">
    <h3><?= h($equiptment->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($equiptment->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($equiptment->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Location') ?></th>
            <td><?= h($equiptment->location) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Quantity') ?></th>
            <td><?= $this->Number->format($equiptment->quantity) ?></td>
        </tr>
    </table>
</div>
