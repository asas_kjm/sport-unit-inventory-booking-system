<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Equiptment[]|\Cake\Collection\CollectionInterface $equiptment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Equiptment'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="equiptment index large-9 medium-8 columns content">
    <h3><?= __('Equiptment') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('location') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($equiptment as $equiptment): ?>
            <tr>
                <td><?= h($equiptment->id) ?></td>
                <td><?= h($equiptment->name) ?></td>
                <td><?= $this->Number->format($equiptment->quantity) ?></td>
                <td><?= h($equiptment->location) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $equiptment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $equiptment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $equiptment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $equiptment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
