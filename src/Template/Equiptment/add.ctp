<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Equiptment $equiptment
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Equiptment'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="equiptment form large-9 medium-8 columns content">
    <?= $this->Form->create($equiptment) ?>
    <fieldset>
        <legend><?= __('Add Equiptment') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('quantity');
            echo $this->Form->control('location');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
