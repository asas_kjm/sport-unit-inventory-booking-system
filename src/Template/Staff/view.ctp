<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff $staff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Staff'), ['action' => 'edit', $staff->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Staff'), ['action' => 'delete', $staff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $staff->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Staff'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Borrow Staff'), ['controller' => 'BorrowStaff', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Borrow Staff'), ['controller' => 'BorrowStaff', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="staff view large-9 medium-8 columns content">
    <h3><?= h($staff->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= h($staff->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($staff->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PhoneNum') ?></th>
            <td><?= h($staff->phoneNum) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dept Code') ?></th>
            <td><?= h($staff->dept_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Position') ?></th>
            <td><?= h($staff->position) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($staff->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Salary') ?></th>
            <td><?= $this->Number->format($staff->salary) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Borrow Staff') ?></h4>
        <?php if (!empty($staff->borrow_staff)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Staff Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($staff->borrow_staff as $borrowStaff): ?>
            <tr>
                <td><?= h($borrowStaff->id) ?></td>
                <td><?= h($borrowStaff->date) ?></td>
                <td><?= h($borrowStaff->staff_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BorrowStaff', 'action' => 'view', $borrowStaff->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BorrowStaff', 'action' => 'edit', $borrowStaff->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BorrowStaff', 'action' => 'delete', $borrowStaff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrowStaff->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
