<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff $staff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Staff'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Borrow Staff'), ['controller' => 'BorrowStaff', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Borrow Staff'), ['controller' => 'BorrowStaff', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="staff form large-9 medium-8 columns content">
    <?= $this->Form->create($staff) ?>
    <fieldset>
        <legend><?= __('Add Staff') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('phoneNum');
            echo $this->Form->control('dept_code');
            echo $this->Form->control('salary');
            echo $this->Form->control('position');
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
