<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BorrowingStud Controller
 *
 * @property \App\Model\Table\BorrowingStudTable $BorrowingStud
 *
 * @method \App\Model\Entity\BorrowingStud[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BorrowingStudController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['BorrowStud', 'Equiptment']
        ];
        $borrowingStud = $this->paginate($this->BorrowingStud);

        $this->set(compact('borrowingStud'));
    }

    /**
     * View method
     *
     * @param string|null $id Borrowing Stud id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $borrowingStud = $this->BorrowingStud->get($id, [
            'contain' => ['BorrowStud', 'Equiptment']
        ]);

        $this->set('borrowingStud', $borrowingStud);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $borrowingStud = $this->BorrowingStud->newEntity();
        if ($this->request->is('post')) {
            $borrowingStud = $this->BorrowingStud->patchEntity($borrowingStud, $this->request->getData());
            if ($this->BorrowingStud->save($borrowingStud)) {
                $this->Flash->success(__('The borrowing stud has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrowing stud could not be saved. Please, try again.'));
        }
        $borrowStud = $this->BorrowingStud->BorrowStud->find('list', ['limit' => 200]);
        $equiptment = $this->BorrowingStud->Equiptment->find('list', ['limit' => 200]);
        $this->set(compact('borrowingStud', 'borrowStud', 'equiptment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Borrowing Stud id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $borrowingStud = $this->BorrowingStud->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $borrowingStud = $this->BorrowingStud->patchEntity($borrowingStud, $this->request->getData());
            if ($this->BorrowingStud->save($borrowingStud)) {
                $this->Flash->success(__('The borrowing stud has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrowing stud could not be saved. Please, try again.'));
        }
        $borrowStud = $this->BorrowingStud->BorrowStud->find('list', ['limit' => 200]);
        $equiptment = $this->BorrowingStud->Equiptment->find('list', ['limit' => 200]);
        $this->set(compact('borrowingStud', 'borrowStud', 'equiptment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Borrowing Stud id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $borrowingStud = $this->BorrowingStud->get($id);
        if ($this->BorrowingStud->delete($borrowingStud)) {
            $this->Flash->success(__('The borrowing stud has been deleted.'));
        } else {
            $this->Flash->error(__('The borrowing stud could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
