<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BorrowStaff Controller
 *
 * @property \App\Model\Table\BorrowStaffTable $BorrowStaff
 *
 * @method \App\Model\Entity\BorrowStaff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BorrowStaffController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Staff']
        ];
        $borrowStaff = $this->paginate($this->BorrowStaff);

        $this->set(compact('borrowStaff'));
    }

    /**
     * View method
     *
     * @param string|null $id Borrow Staff id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $borrowStaff = $this->BorrowStaff->get($id, [
            'contain' => ['Staff']
        ]);

        $this->set('borrowStaff', $borrowStaff);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $borrowStaff = $this->BorrowStaff->newEntity();
        if ($this->request->is('post')) {
            $borrowStaff = $this->BorrowStaff->patchEntity($borrowStaff, $this->request->getData());
            if ($this->BorrowStaff->save($borrowStaff)) {
                $this->Flash->success(__('The borrow staff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrow staff could not be saved. Please, try again.'));
        }
        $staff = $this->BorrowStaff->Staff->find('list', ['limit' => 200]);
        $this->set(compact('borrowStaff', 'staff'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Borrow Staff id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $borrowStaff = $this->BorrowStaff->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $borrowStaff = $this->BorrowStaff->patchEntity($borrowStaff, $this->request->getData());
            if ($this->BorrowStaff->save($borrowStaff)) {
                $this->Flash->success(__('The borrow staff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrow staff could not be saved. Please, try again.'));
        }
        $staff = $this->BorrowStaff->Staff->find('list', ['limit' => 200]);
        $this->set(compact('borrowStaff', 'staff'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Borrow Staff id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $borrowStaff = $this->BorrowStaff->get($id);
        if ($this->BorrowStaff->delete($borrowStaff)) {
            $this->Flash->success(__('The borrow staff has been deleted.'));
        } else {
            $this->Flash->error(__('The borrow staff could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
