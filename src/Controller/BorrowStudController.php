<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BorrowStud Controller
 *
 * @property \App\Model\Table\BorrowStudTable $BorrowStud
 *
 * @method \App\Model\Entity\BorrowStud[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BorrowStudController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Student']
        ];
        $borrowStud = $this->paginate($this->BorrowStud);

        $this->set(compact('borrowStud'));
    }

    /**
     * View method
     *
     * @param string|null $id Borrow Stud id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $borrowStud = $this->BorrowStud->get($id, [
            'contain' => ['Student']
        ]);

        $this->set('borrowStud', $borrowStud);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $borrowStud = $this->BorrowStud->newEntity();
        if ($this->request->is('post')) {
            $borrowStud = $this->BorrowStud->patchEntity($borrowStud, $this->request->getData());
            if ($this->BorrowStud->save($borrowStud)) {
                $this->Flash->success(__('The borrow stud has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrow stud could not be saved. Please, try again.'));
        }
        $student = $this->BorrowStud->Student->find('list', ['limit' => 200]);
        $this->set(compact('borrowStud', 'student'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Borrow Stud id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $borrowStud = $this->BorrowStud->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $borrowStud = $this->BorrowStud->patchEntity($borrowStud, $this->request->getData());
            if ($this->BorrowStud->save($borrowStud)) {
                $this->Flash->success(__('The borrow stud has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrow stud could not be saved. Please, try again.'));
        }
        $student = $this->BorrowStud->Student->find('list', ['limit' => 200]);
        $this->set(compact('borrowStud', 'student'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Borrow Stud id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $borrowStud = $this->BorrowStud->get($id);
        if ($this->BorrowStud->delete($borrowStud)) {
            $this->Flash->success(__('The borrow stud has been deleted.'));
        } else {
            $this->Flash->error(__('The borrow stud could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
