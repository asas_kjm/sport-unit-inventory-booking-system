<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Equiptment Controller
 *
 * @property \App\Model\Table\EquiptmentTable $Equiptment
 *
 * @method \App\Model\Entity\Equiptment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EquiptmentController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $equiptment = $this->paginate($this->Equiptment);

        $this->set(compact('equiptment'));
    }

    /**
     * View method
     *
     * @param string|null $id Equiptment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $equiptment = $this->Equiptment->get($id, [
            'contain' => []
        ]);

        $this->set('equiptment', $equiptment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $equiptment = $this->Equiptment->newEntity();
        if ($this->request->is('post')) {
            $equiptment = $this->Equiptment->patchEntity($equiptment, $this->request->getData());
            if ($this->Equiptment->save($equiptment)) {
                $this->Flash->success(__('The equiptment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The equiptment could not be saved. Please, try again.'));
        }
        $this->set(compact('equiptment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Equiptment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $equiptment = $this->Equiptment->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $equiptment = $this->Equiptment->patchEntity($equiptment, $this->request->getData());
            if ($this->Equiptment->save($equiptment)) {
                $this->Flash->success(__('The equiptment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The equiptment could not be saved. Please, try again.'));
        }
        $this->set(compact('equiptment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Equiptment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $equiptment = $this->Equiptment->get($id);
        if ($this->Equiptment->delete($equiptment)) {
            $this->Flash->success(__('The equiptment has been deleted.'));
        } else {
            $this->Flash->error(__('The equiptment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
