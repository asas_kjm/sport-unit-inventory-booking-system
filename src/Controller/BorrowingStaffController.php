<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * BorrowingStaff Controller
 *
 * @property \App\Model\Table\BorrowingStaffTable $BorrowingStaff
 *
 * @method \App\Model\Entity\BorrowingStaff[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BorrowingStaffController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Equiptment']
        ];
        $borrowingStaff = $this->paginate($this->BorrowingStaff);

        $this->set(compact('borrowingStaff'));
    }

    /**
     * View method
     *
     * @param string|null $id Borrowing Staff id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $borrowingStaff = $this->BorrowingStaff->get($id, [
            'contain' => ['Equiptment']
        ]);

        $this->set('borrowingStaff', $borrowingStaff);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $borrowingStaff = $this->BorrowingStaff->newEntity();
        if ($this->request->is('post')) {
            $borrowingStaff = $this->BorrowingStaff->patchEntity($borrowingStaff, $this->request->getData());
            if ($this->BorrowingStaff->save($borrowingStaff)) {
                $this->Flash->success(__('The borrowing staff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrowing staff could not be saved. Please, try again.'));
        }
        $equiptment = $this->BorrowingStaff->Equiptment->find('list', ['limit' => 200]);
        $this->set(compact('borrowingStaff', 'equiptment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Borrowing Staff id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $borrowingStaff = $this->BorrowingStaff->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $borrowingStaff = $this->BorrowingStaff->patchEntity($borrowingStaff, $this->request->getData());
            if ($this->BorrowingStaff->save($borrowingStaff)) {
                $this->Flash->success(__('The borrowing staff has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrowing staff could not be saved. Please, try again.'));
        }
        $equiptment = $this->BorrowingStaff->Equiptment->find('list', ['limit' => 200]);
        $this->set(compact('borrowingStaff', 'equiptment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Borrowing Staff id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $borrowingStaff = $this->BorrowingStaff->get($id);
        if ($this->BorrowingStaff->delete($borrowingStaff)) {
            $this->Flash->success(__('The borrowing staff has been deleted.'));
        } else {
            $this->Flash->error(__('The borrowing staff could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
